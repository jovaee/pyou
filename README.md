# pyou

A python application that integrates with VLC Media Player and Youtube to play a Youtube playlist through VLC. 
It support the pausing of, skipping forward and backward through the items in the playlist, essentially 
allowing you to control Youtube through your keyboard.

#### Features
 * Pausing/Playing items
 * Skipping forward and backward through the playlist
 * Sequential playing of items in playlist
 * Shuffling: Playing the items in a random order
 
##### Shuffling
Shuffling was implemented to how I want shuffling to work. Most of the times I put playlists on shuffle
and then hear the same song being played over and over which really annoyed me. This implementation of shuffle
takes a `unique_count` number that specifies the number of unique items that must be played in a row.
If this number is set to `20` then you will not hear the same song for at least 20 items. As soon
as this number has been reached then the oldest item played will be eligible to be played again.


### Requirments
Simply run to install all the requirements for the project.

    $ pip install -r requirements.txt
    
Then also VLC Media player is required. Make sure to install the same bit version of VLC
as the Python that is installed ie 32-bit VLC and Python or 64-bit VLC and Python.
Mixing the bit versions does not work.
 
Also a Youtube API key will be required to allow this to access Youtube. This can be generated
on the Google Developer Console and be put in a file called `secret.json` that will look something
like:

    # secret.json
    {
        "api_key": "KeyThatWasGenerated"
    }


### Planned Features

* Some kind of preloading to increase the speed of cycling through items.
* Change volume during runtime.
* Allow resuming from last session. Remembers what item was played last, maybe entire playlist.
* Be able to select individual item during runtime.