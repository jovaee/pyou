import json
import random
import time
import sys

import click
import pafy
import vlc

from enum import IntEnum
from sys import platform

from colorama import init
from pynput import keyboard
from termcolor import colored

from clients.youtube import YouTubeClient

ASCII = """
  __________                     
  \______   \___.__. ____  __ __ 
   |     ___<   |  |/  _ \|  |  \\
   |    |    \___  (  <_> )  |  /
   |____|    / ____|\____/|____/ 
             \/                  
"""
# Ref http://tldp.org/HOWTO/Bash-Prompt-HOWTO/x361.html
ERASE_LINE = "\033[K"

PYOU_COLOR = "yellow"
WARNING_COLOR = "red"
ACCENT_COLOR = "yellow"

# Init colorama
init(autoreset=True)


def create_youtube_client() -> YouTubeClient:
    """
    Create YouTube client instance
    """
    with open('secret.json', 'r') as secret:
        api_key = json.load(secret)['api_key']
        return YouTubeClient(api_key)


class PyouPlayer:
   
    # YouTube Client
    client = create_youtube_client()

    def __init__(self, playlist: str, volume: int = 100, shuffle: bool = False, unique_count: int = 25):
        """
        :param playlist: YouTube Playlist ID
        :param volume: Volume the player will play at. Independent of system volume settings
        :param shuffle: Should random videos be selected from playlist during playback
        :param unique_count: The number of unique videos that should be played after each other when shuffling
        """
        self.playlist = playlist
        self.volume = volume
        self.shuffle = shuffle
        self.unique_count = unique_count
        self.shuffle_index = 0  # Used to calculate `video_index` when MM_PREV/MM_NEXT used
        self.previous_played_unique_indices = []  # Used to keep record of unique videos played with `unique_count`
        self.previous_played_indices = []  # Keeps a record of videos played as they are shuffled. Allows MM_PREV/MM_NEXT to play correctly
        self.videos = []

        self.next_requested = False
        self.previous_requested = False
        self.video_index = 0  # The current index of the song being played

        self.player = vlc.MediaPlayer()
        self.player.audio_set_volume(volume)

    def MM_STOP_PRESSED(self):
        self.player.stop()

    def MM_PREVIOUS_PRESSED(self):
        self.previous_requested = True

    def MM_PLAY_PRESSED(self):
        self.player.play()

    def MM_PAUSE_PLAY_PRESSED(self):
        self.player.pause()

    def MM_NEXT_PRESSED(self):
        self.next_requested = True

    @staticmethod
    def get_progressbar_string(current_time: int, total_time: int, progressbar_length: int = 50, fill: str = '█') -> str:
        """
        Get a progress bar string. This string will be formatted as follows:
            00:12:53 |██████  | -00:00:01
        The current time on the left, progress bar showing percentage complete and then remaining time.
        :param current_time: Current seek of video in ms
        :param total_time: Total length of video in ms
        :param progressbar_length: Number of characters that progress bar should consit of. The width
        :param fill: Character to use to fill filled part of progress bar
        """
        percent = current_time / total_time
        number_filled = round(progressbar_length * percent)
        number_empty = progressbar_length - number_filled

        remaining_time = total_time // 1000 - current_time // 1000
        remaining_time_hours = remaining_time // 3600
        remaining_time_minutes = (remaining_time - (remaining_time_hours * 3600)) // 60
        remaining_time_seconds = remaining_time - (remaining_time_hours * 3600) - (remaining_time_minutes * 60)
        remaining_time_str = f"{str(remaining_time_hours).zfill(2)}:{str(remaining_time_minutes).zfill(2)}:{str(remaining_time_seconds).zfill(2)}"

        current_time = current_time // 1000
        current_time_hours = current_time // 3600
        current_time_minutes = (current_time - (current_time_hours * 3600)) // 60
        current_time_seconds = current_time - (current_time_hours * 3600) - (current_time_minutes * 60)
        current_time_str = f"{str(current_time_hours).zfill(2)}:{str(current_time_minutes).zfill(2)}:{str(current_time_seconds).zfill(2)}"

        pgs = f"{current_time_str} |{colored(fill * number_filled, ACCENT_COLOR)}{' ' * number_empty}| -{remaining_time_str}"
        return pgs
            
    def calculate_correct_next_video_index(self, initial: bool = False):
        """
        Calculates the correct next video index based on state.
        :param initial: Set to True if this is the first time a video index must be selected
        """
        if self.shuffle:
            if self.shuffle_index == len(self.previous_played_indices):
                index = random.randint(0, len(self.videos))
                while index in self.previous_played_unique_indices:
                    index = random.randint(0, len(self.videos))

                self.video_index = index
            else:
                self.video_index = self.previous_played_indices[self.shuffle_index]
        elif not initial:
            self.video_index += 1

    def loop(self):
        """
        The main loop of the player.
        """
        print('Retrieving playlist videos...', end='\r')
        self.videos = self.client.get_playlist_videos(self.playlist)
        if not self.videos:
            print('Please make sure that the playlist URL is valid and that it has videos in it')
            sys.exit(0)
        
        # Make sure that unique_count is never more than the total number of videos
        if self.unique_count > len(self.videos):
            print(f'{colored("WARNING", WARNING_COLOR)}: {colored("--unique-count", ACCENT_COLOR)} provided is less than the number of videos in the playlist. Limiting {colored("--unique-count", ACCENT_COLOR)} to {len(self.videos)}')
            self.unique_count = len(self.videos)

        while True:  # Keep the thread running

            # If shuffle is on at the start then get a random start song
            self.calculate_correct_next_video_index(initial=True)

            while self.video_index < len(self.videos):  # Go though all the urls
                if len(self.previous_played_unique_indices) == self.unique_count:
                    # We reached the number of uniques songs that must be played in a row
                    # so remove the oldest entry
                    self.previous_played_unique_indices.pop(0)

                if self.shuffle and not self.previous_requested and self.shuffle_index == len(self.previous_played_indices):
                    self.previous_played_indices.append(self.video_index)
                    self.previous_played_unique_indices.append(self.video_index)

                url = f'https://www.youtube.com/watch?v={self.videos[self.video_index]["url"]}'

                video = pafy.new(url)
                audio = video.getbestaudio()
                media = vlc.Media(audio.url)

                self.player.set_media(media)
                self.player.play()

                # If a next_requested or previous_requested we need to remove the print notification that was printed when
                # buttons were initially pressed
                if any([self.next_requested, self.previous_requested]):
                    print(ERASE_LINE, end='\r')

                self.next_requested = False
                self.previous_requested = False

                try:
                    print(f'{ERASE_LINE}[{colored(f"{self.video_index:>4}", ACCENT_COLOR)}] Playing "{video.title}" from "{video.author}" ({video.duration})')
                except Exception:
                    pass

                while self.player.get_state() not in [vlc.State.Ended, vlc.State.Error] and not any([self.next_requested, self.previous_requested]):
                    if self.player.get_length():  # For when video hasn't complety loaded into VLC and has not length yet
                        pgs = self.get_progressbar_string(self.player.get_time(), self.player.get_length())
                        print(f"{ERASE_LINE}{pgs}", end='\r')
                    
                    time.sleep(0.5)

                if any([self.next_requested, self.previous_requested]):
                    print(colored(f'{ERASE_LINE}Changing to {"next" if self.next_requested else "previous"} video...', ACCENT_COLOR), end='\r')
                else:
                    print(ERASE_LINE, end='\r')

                if self.next_requested:
                    if self.shuffle:
                        self.shuffle_index += 1
                        self.calculate_correct_next_video_index()
                    else:
                        self.video_index += 1
                elif self.previous_requested:
                    if self.shuffle:
                        self.shuffle_index = max(0, self.shuffle_index - 1)  # Make sure to not go negative
                        self.video_index = self.previous_played_indices[self.shuffle_index]
                    else:
                        self.video_index = max(0, self.video_index - 1)  # Make sure to not go negative
                else:
                    self.shuffle_index += 1
                    self.calculate_correct_next_video_index()
    
    def run(self):
        """
        Start the event-loop for the player
        """
        def on_press(key):
            try:
                if key == keyboard.Key.f6 :
                    self.MM_PREVIOUS_PRESSED()
                elif key == keyboard.Key.f7:
                    self.MM_PAUSE_PLAY_PRESSED()
                elif key == keyboard.Key.f8:
                    self.MM_NEXT_PRESSED()
            except AttributeError:
                # A special key was pressed
                pass

        # Collect events until released
        with keyboard.Listener(on_press=on_press) as listener:
            self.loop()
            listener.join()


@click.command()
@click.option('--playlist', type=str, help='YouTube Playlist ID')
@click.option('--shuffle', default=False, type=bool, help='Shuffle playlist')
@click.option('--volume', default=100, type=click.IntRange(0, 100, clamp=True), help='Player volume')
@click.option('--unique-count', default=25, type=int, help='Number of unique items to be played when suffling')
def main(playlist: str, shuffle: bool, volume: int, unique_count: int):
    # Print the wonderful logo ASCII
    print(colored(ASCII, PYOU_COLOR))

    player = PyouPlayer(playlist, volume, shuffle, unique_count)
    player.run()


if __name__ == "__main__":
    main()
